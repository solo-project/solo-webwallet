let global : any = typeof window !== 'undefined' ? window : self;
global.config = {
	apiUrl:typeof window !== 'undefined' && window.location ? window.location.href.substr(0,window.location.href.lastIndexOf('/')+1)+'api/' : 'https://www.masariwallet.com/api/',
	trustedDaemonsAddresses:[
		'https://wallet.minesolo.com:22424/'
	],
	phpRelay:typeof window !== 'undefined' ? true : false,
	mainnetExplorerUrl: "https://explorer.minesolo.com/",
	mainnetExplorerUrlHash: "https://explorer.minesolo.com/tx/{ID}",
	mainnetExplorerUrlBlock: "https://explorer.minesolo.com/block/{ID}",
	testnetExplorerUrl: "http://testnet.explorer.minesolo.com/",
	testnetExplorerUrlHash: "http://testnet.explorer.minesolo.com/tx/{ID}",
	testnetExplorerUrlBlock: "http://testnet.explorer.minesolo.com/block/{ID}",
	testnet: false,
	coinUnitPlaces: 9,
	txMinConfirms: 20,         // corresponds to CRYPTONOTE_DEFAULT_TX_SPENDABLE_AGE in Monero
	txCoinbaseMinConfirms: 1000, // corresponds to CRYPTONOTE_MINED_MONEY_UNLOCK_WINDOW in Monero
	addressPrefix: 13975,
	integratedAddressPrefix: 26009,
	addressPrefixTestnet: 13976,
	integratedAddressPrefixTestnet: 26112,
	subAddressPrefix: 23578,
	subAddressPrefixTestnet: 27947,
	feePerKB: new JSBigInt('400000000'),//20^10 - for testnet its not used, as fee is dynamic.
	dustThreshold: new JSBigInt('1000000000'),//10^10 used for choosing outputs/change - we decompose all the way down if the receiver wants now regardless of threshold
	defaultMixin: 13, // default value mixin

	idleTimeout: 30,
	idleWarningDuration: 20,

	coinSymbol: 'XSL',
	openAliasPrefix: "xsl",
	coinName: 'Solo',
	coinUriPrefix: 'solo:',
	avgBlockTime: 20,
	maxBlockNumber: 500000000,

//	donationAddresses : [
//		'5qfrSvgYutM1aarmQ1px4aDiY9Da7CLKKDo3UkPuUnQ7bT7tr7i4spuLaiZwXG1dFQbkCinRUNeUNLoNh342sVaqTaWqvt8',
//		'5nYWvcvNThsLaMmrsfpRLBRou1RuGtLabUwYH7v6b88bem2J4aUwsoF33FbJuqMDgQjpDRTSpLCZu3dXpqXicE2uSWS4LUP',
//		'9ppu34ocgmeZiv4nS2FyQTFLL5wBFQZkhAfph7wGcnFkc8fkCgTJqxnXuBkaw1v2BrUW7iMwKoQy2HXRXzDkRE76Cz7WXkD'
//	]
};
